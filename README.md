# Android录音二级缓存

#### 项目介绍
有时候使用AudioRecord录制的音频文件播放时发现速度很快如同慢录快放，常见原因是：不断的audioRecord.read(buffer, 0, bufferSize);将数据写入buffer时，其中的原数据bos.write(buffer);还没有完成，导致写入文件的数据有丢失。

#### 软件架构
本例代码通过两个线程安全的List保存buffer，一个负责供audioRecord缓存，一个负责供bos读来写文件，当录制操作停止后，处理List剩余的数据。


#### 核心代码-录制入口线程
``` java
class RecordThread extends Thread {
    private boolean isDestroy = false; 
    private final File tmpPcmFile; 

    public RecordThread() { 
        tmpPcmFile = new File("/sdcard/test.pcm");
         
        try {
            tmpPcmFile.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDestroy(boolean isDestroy) {
        this.isDestroy = isDestroy;
    }

    @Override
    public void run() {

        audioRecord.startRecording(); // audioRecord初始化代码略

        PcmBufferRunnable bufferRunnable = new PcmBufferRunnable(tmpPcmFile, onPcmBufferFinishListener);
        new Thread(bufferRunnable).start();

        while (!isDestroy) {
            byte[] buffer = new byte[bufferSize];
            int length = audioRecord.read(buffer, 0, bufferSize);
            bufferRunnable.add(buffer); // 加入到两个缓存
            }
        }

        audioRecord.stop();

        bufferRunnable.disableWriting();
    }

    OnPcmBufferFinishListener onPcmBufferFinishListener = new OnPcmBufferFinishListener() {
        @Override
        public void onPcmBufferFinish() {
            Log.i("ard", "onPcmBufferFinish");
            // 到这里缓存处理完毕，tmpPcmFile可以读取了
        }
    };
}
```


#### 核心代码-数据读写二级缓存线程
``` java
class PcmBufferRunnable implements Runnable {
    private List<byte[]> bufferListSession = Collections.synchronizedList(new ArrayList<byte[]>()); // 第二级
    private List<byte[]> bufferListCache = Collections.synchronizedList(new ArrayList<byte[]>()); // 第一级
    private OnPcmBufferFinishListener onPcmBufferFinishListener;
    private boolean writingAbble = true;
    private BufferedOutputStream bos;

    public PcmBufferRunnable(File tmpPcmFile, OnPcmBufferFinishListener listener) {
        onPcmBufferFinishListener = listener;

        try {
            FileOutputStream out = new FileOutputStream(tmpPcmFile.getAbsolutePath());
            bos = new BufferedOutputStream(out);
        } catch (Exception e) {
        }
    }

    public void disableWriting() {
        writingAbble = false;
    }

    public void add(byte[] bts) {
        bufferListSession.add(bts);
        if (bufferListCache.size() < 1) {
            bufferListCache.addAll(bufferListSession);
            bufferListSession.clear();
        }
    }

    @Override
    public void run() {
        int size;
        while (writingAbble) {
            size = bufferListCache.size();
            if (null != bos && size > 0) {
                try {
                    byte[] bts = bufferListCache.remove(0);

                    Thread.sleep(100); // ---------------------模拟io延迟

                    bos.write(bts);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        //////////////////////////////////////// 读取第二级缓存

        if (bufferListSession.size() > 0) {
            bufferListCache.addAll(bufferListSession);
            bufferListSession.clear();
        }

        size = bufferListCache.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                byte[] bts = bufferListCache.get(i);
                if (null != bos) {
                    try {
                        bos.write(bts);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        if (bos != null) {
            try {
                bos.flush();
                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bos = null;
        bufferListCache.clear();

        if (null != onPcmBufferFinishListener) {
            onPcmBufferFinishListener.onPcmBufferFinish();
        }
    }
}
```


#### 核心代码-接口
``` java

interface OnPcmBufferFinishListener {
    void onPcmBufferFinish();
}

```
